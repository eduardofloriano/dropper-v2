package br.com.dropper.soa.modelocanonico.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import br.com.dropper.soa.modelocanonico.ErroSoaEnum;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DropperSoaFault", propOrder = {
    "codigo", "erro", "mensagem", "instrucoes"//, "stackTrace"
})
public class DropperSoaFault {
	ErroSoaEnum erro;
	String mensagem;
	String instrucoes;
	//Exception stackTrace;

	
	public DropperSoaFault() {
		
	}
	
	public DropperSoaFault(ErroSoaEnum erro, String mensagem, String instrucoes) {
		this.erro = erro;
		this.mensagem = mensagem;
		this.instrucoes = instrucoes;
	}


	@XmlElement(name="codigo")
	public int getCodigo() {
		return erro.getCodigo();
	}

	public ErroSoaEnum getErro() {
		return erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public String getInstrucoes() {
		return instrucoes;
	}
	
	

	public void setCodigo(int codigo) {
		this.erro = ErroSoaEnum.fromCodigo(codigo);
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public void setInstrucoes(String instrucoes) {
		this.instrucoes = instrucoes;
	}




}
