package br.com.dropper.soa.modelocanonico.exception;

import javax.xml.ws.WebFault;

import br.com.dropper.soa.modelocanonico.ErroSoaEnum;
import br.com.dropper.soa.modelocanonico.fault.DropperSoaFault;

@WebFault(name="ErroTecnicoFault")
public class ProblemaInesperadoException extends AbstractSoaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6270528102043630461L;

	public ProblemaInesperadoException(String message, DropperSoaFault fault,
			Throwable cause) {
		super(message, fault, cause);
	}

	public ProblemaInesperadoException(String message, DropperSoaFault fault) {
		super(message, fault);
	}
	
	public ProblemaInesperadoException() {
		super("Problema Inesperado", new DropperSoaFault(ErroSoaEnum.ProblemaInesperado, "Problema Inesperado.", "Entrar em contato com suporte técnico fornecendo detalhes."));
		//super.getFaultInfo().setStackTrace(this);
	}

	
	
}
