package br.com.dropper.soa.modelocanonico;


public enum ErroSoaEnum {
		ParametroObrigatorioNaoInformado(1)
	
	// Erros de Entidade Não Localizada
	,	ImagemNaoLocalizado(1000)
	
	// Erros Técnicos	
	,	ProblemaInesperado(99999);
		
		
	private int codigo;

	private ErroSoaEnum(int codigo) {
		this.codigo = codigo;
	}

	public int getCodigo() {
		return codigo;
	}
	
	public static ErroSoaEnum fromCodigo(int codigo) {
		ErroSoaEnum[] values = ErroSoaEnum.values();
		for (ErroSoaEnum erroSoaEnum : values) {
			if(erroSoaEnum.codigo == codigo) {
				return erroSoaEnum;
			}
		}
		throw new IllegalArgumentException("Erro SOA desconhecido: " + codigo);
	}
	

}
