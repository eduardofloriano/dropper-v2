package br.com.dropper.soa.modelocanonico.exception;

import br.com.dropper.soa.modelocanonico.fault.DropperSoaFault;

public abstract class AbstractSoaException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2131978993961004893L;
	

	DropperSoaFault faultInfo;

	public AbstractSoaException(String message, DropperSoaFault faultInfo) {
		super(message);
		this.faultInfo = faultInfo;
	}

	public AbstractSoaException(String message, DropperSoaFault faultInfo, Throwable cause) {
		super(message, cause);
		this.faultInfo = faultInfo;
	}

	public DropperSoaFault getFaultInfo() {
		return faultInfo;
	}

	
	
}
