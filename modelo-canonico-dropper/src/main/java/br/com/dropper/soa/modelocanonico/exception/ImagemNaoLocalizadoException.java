package br.com.dropper.soa.modelocanonico.exception;

import javax.xml.ws.WebFault;

import br.com.dropper.soa.modelocanonico.ErroSoaEnum;
import br.com.dropper.soa.modelocanonico.fault.DropperSoaFault;

@WebFault(name = "ImagemNaoLocalizadoFault")
public class ImagemNaoLocalizadoException extends AbstractSoaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1411790665390894632L;

	public ImagemNaoLocalizadoException(String message, DropperSoaFault fault) {
		super(message, fault);
	}

	public ImagemNaoLocalizadoException(String message, DropperSoaFault fault, Throwable cause) {
		super(message, fault, cause);
	}

	public ImagemNaoLocalizadoException(String message) {
		super(message, new DropperSoaFault(ErroSoaEnum.ImagemNaoLocalizado,
				message, "Verifique se o código foi digitado corretamente."));
	}
	
	public ImagemNaoLocalizadoException() {
		super("Imagem nao foi localizado", new DropperSoaFault(ErroSoaEnum.ImagemNaoLocalizado,
				"Imagem nao foi localizado", "Verifique se o código foi digitado corretamente."));
	}
}
