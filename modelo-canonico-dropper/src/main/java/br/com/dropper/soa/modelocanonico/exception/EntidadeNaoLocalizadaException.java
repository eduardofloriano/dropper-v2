package br.com.dropper.soa.modelocanonico.exception;

import br.com.dropper.soa.modelocanonico.fault.DropperSoaFault;

public class EntidadeNaoLocalizadaException extends
		AbstractSoaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6428907255287629295L;
	
	private String entidade;
	private String codigo;

	public EntidadeNaoLocalizadaException(String message, DropperSoaFault fault,
			Throwable cause) {
		super(message, fault, cause);
		
	}

	public EntidadeNaoLocalizadaException(String message, DropperSoaFault fault) {
		super(message, fault);
	}

   public String getEntidade() {
      return entidade;
   }

   public void setEntidade(String entidade) {
      this.entidade = entidade;
   }

   public String getCodigo() {
      return codigo;
   }

   public void setCodigo(String codigo) {
      this.codigo = codigo;
   }



		
}
