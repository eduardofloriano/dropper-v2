package br.com.dropper.soa.modelocanonico.exception;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.dropper.soa.modelocanonico.fault.DropperSoaFault;

public abstract class AbstractSoaExceptionBackup extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2131978993961004893L;
	

	private List<DropperSoaFault> detalhes = new ArrayList<>();

	public AbstractSoaExceptionBackup(DropperSoaFault detalhe) {
		this.detalhes.add(detalhe);
	}

	public AbstractSoaExceptionBackup(DropperSoaFault detalhe, Throwable causedBy) {
		super(causedBy);
		this.detalhes.add(detalhe);
	}

	public AbstractSoaExceptionBackup(DropperSoaFault detalhe, Throwable causedBy, String mensagem) {
		super(mensagem, causedBy);
		this.detalhes.add(detalhe);
	}

	public AbstractSoaExceptionBackup(List<DropperSoaFault> detalhes) {
		this.detalhes.addAll(detalhes);
	}

	public AbstractSoaExceptionBackup(List<DropperSoaFault> detalhes, Throwable causedBy) {
		super(causedBy);
		this.detalhes.addAll(detalhes);
	}

	public AbstractSoaExceptionBackup(List<DropperSoaFault> detalhes, Throwable causedBy, String mensagem) {
		super(mensagem, causedBy);
		this.detalhes.addAll(detalhes);
	}

	public List<DropperSoaFault> getDetalhes() {
		return Collections.unmodifiableList(detalhes);
	}
	
	
}
