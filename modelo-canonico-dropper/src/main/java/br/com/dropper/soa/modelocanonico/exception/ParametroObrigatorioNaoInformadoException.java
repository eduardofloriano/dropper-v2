package br.com.dropper.soa.modelocanonico.exception;

import javax.xml.ws.WebFault;

import br.com.dropper.soa.modelocanonico.ErroSoaEnum;
import br.com.dropper.soa.modelocanonico.fault.DropperSoaFault;

@WebFault(name="ParametroObrigatorioNaoInformadoFault")
public class ParametroObrigatorioNaoInformadoException extends AbstractSoaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6270528102043630461L;

	public ParametroObrigatorioNaoInformadoException(String message, DropperSoaFault fault,
			Throwable cause) {
		super(message, fault, cause);
	}

	public ParametroObrigatorioNaoInformadoException(String message, DropperSoaFault fault) {
		super(message, fault);
	}
	
	public ParametroObrigatorioNaoInformadoException() {
		super("Parâmetro Obrigatório Não Informado", new DropperSoaFault(ErroSoaEnum.ParametroObrigatorioNaoInformado, "Parâmetro Obrigatório Não Informado.", "Verificar os parâmetros informados antes de tentar novamente."));
	}

}
