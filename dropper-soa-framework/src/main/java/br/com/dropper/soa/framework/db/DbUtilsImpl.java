package br.com.dropper.soa.framework.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.dropper.soa.framework.log.ILogStrategy;
import br.com.dropper.soa.framework.log.Log4J;

@Singleton
public class DbUtilsImpl implements DbUtils {
	@Inject
	@Log4J
	ILogStrategy log;
	
	public void closeQuietly(Connection c, ResultSet rs, Statement stmt) {
		closeQuietly(c, stmt, rs);
	}

	public void closeQuietly(Connection c, Statement stmt, ResultSet rs) {
		try {
			if(stmt != null) {
				stmt.close();
			}
		} catch(Exception e) {
			log.warn("Não foi possível encerrar o statement devido a:" + e.getMessage(), e);
		}
		try {
			if(rs != null) {
				rs.close();
			}
		} catch(Exception e) {
			log.warn("Não foi possível encerrar o resultSet devido a:" + e.getMessage(), e);
		}
		try {
			if(c != null) {
				c.close();
			}
		} catch(Exception e) {
			log.warn("Não foi possível encerrar a conexão devido a:" + e.getMessage(), e);
		}
	}
	
	@Override
	public void closeQuietly(Connection c) {
		closeQuietly(c, (Statement) null, null);
	}

	@Override
	public void closeQuietly(Connection c, Statement stmt) {
		closeQuietly(c, stmt, null);
	}


	@Override
	public void closeQuietly(Statement stmt, ResultSet rs) {
		closeQuietly(null, stmt, rs);
		
	}

	@Override
	public void closeQuietly(ResultSet rs) {
		closeQuietly(null, (Statement)null, null);
	}

   @Override
   public void closeQuietly(Statement stmt) {
      closeQuietly((Connection)null, stmt, null);
   }

   @Override
   public void rollbackQuietly(Connection c) {
      try {
         c.rollback();
      } catch(Exception e) {
         log.warn("Ocorreu um erro durante o rollback: " + e.getMessage(), e);
      }
      
   }
}
