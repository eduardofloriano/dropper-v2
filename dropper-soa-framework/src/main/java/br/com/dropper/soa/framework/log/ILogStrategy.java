package br.com.dropper.soa.framework.log;

import javax.inject.Singleton;

@Singleton
public interface ILogStrategy {

	public void debug(String message);
	public void debug(String message, Throwable e);

	public void info(String message);
	public void info(String message, Throwable e);

	public void warn(String message);
	public void warn(String message, Throwable e);

	public void error(String message);
	public void error(String message, Throwable e);


	public void fatal(String message);
	public void fatal(String message, Throwable ex);
}
