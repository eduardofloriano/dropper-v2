package br.com.dropper.soa.framework.log;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LogWriter {

	@Inject
	@Log4J
	private ILogStrategy _logStrategy;

	public void debug(String message) {
		_logStrategy.debug("	- " + message);
	}

	public void info(String message) {
		_logStrategy.info("	*** " + message);
	}

	public void warn(String message) {
		_logStrategy.warn("	!!! " + message);
	}

	public void error(String message) {
		_logStrategy.error("-------------------------ERRO--------------------------------------");
		_logStrategy.error(message);
		_logStrategy.error("-------------------------ERRO--------------------------------------");
	}

	public void error(String message, Exception ex) {
		_logStrategy.error("-------------------------ERRO--------------------------------------");
		_logStrategy.error(message, ex);
		_logStrategy.error("-------------------------ERRO--------------------------------------");
	}

	public void fatal(String message) {
		_logStrategy.fatal("-------------------------!!! FATAL !!!--------------------------------------");
		_logStrategy.error(message);
		_logStrategy.fatal("-------------------------!!! FATAL !!!--------------------------------------");
	}

	public void fatal(String message, Exception ex) {
		_logStrategy.fatal("-------------------------!!! FATAL !!!--------------------------------------");
		_logStrategy.error(message, ex);
		_logStrategy.fatal("-------------------------!!! FATAL !!!--------------------------------------");
	}
}
