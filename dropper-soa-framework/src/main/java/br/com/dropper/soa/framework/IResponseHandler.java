package br.com.dropper.soa.framework;

import javax.ws.rs.core.Response;

public interface IResponseHandler {
	
	public Response buildFault(String tipo, String codigo, String mensagem, String instrucao, Throwable e);
	public Response buildFault(String tipo, String codigo, String mensagem, String instrucao, String detalhe);
	
	public Response buildTechnicalFault(String codigo, Throwable e);
	
	public Response buildTodosParametrosObrigatoriosResponse(String codigo, String... parametrosObrigatorios);
	public Response buildAoMenosUmParametroObrigatorioResponse(String codigo, String... parametrosObrigatorios);
}
