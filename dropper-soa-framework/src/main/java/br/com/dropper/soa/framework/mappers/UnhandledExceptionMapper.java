package br.com.dropper.soa.framework.mappers;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.dropper.soa.framework.IResponseHandler;
import br.com.dropper.soa.framework.log.ILogStrategy;
import br.com.dropper.soa.framework.log.Log4J;

@Provider
public class UnhandledExceptionMapper implements ExceptionMapper<Throwable> {

	@Inject
	IResponseHandler responseHandler;
	
	@Inject
	@Log4J
	ILogStrategy log;
	
	
	@Override
	public Response toResponse(Throwable e) {
		log.warn(e.getMessage(), e); 
		return responseHandler.buildTechnicalFault("TE-0001", e);
	}

}
