package br.com.dropper.soa.framework.log;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.apache.log4j.Logger;

public interface ILogProducer {

	@Produces
	public Logger produceLogger(InjectionPoint injectionPoint);
}
