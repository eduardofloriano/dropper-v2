package br.com.dropper.soa.framework;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;


public final class ServiceFactory<T> {


    private Class<T> seiClass;
	private String serviceName;
	private String serviceNamespace;
	private String serviceContextRoot;
	private static final int TOTAL_DE_TIPOS_DE_SERVICES = 2;
	
	public ServiceFactory(	
	  Class<T> seiClass,
	  String serviceContextRoot, String serviceName, String serviceNamespace) {		
		this.seiClass = seiClass;
		this.serviceContextRoot = serviceContextRoot;
		this.serviceName = serviceName;
		this.serviceNamespace = serviceNamespace;
	}
	

	public T lookupService() throws ConfigurationException, MalformedURLException {
		
		String endPointUrl = getServiceEndpointUrl();
		String wsdlURLStr = endPointUrl + "/" + serviceContextRoot + "/" + serviceName + "?wsdl";
		URL wsdlURL = new URL(wsdlURLStr);
		QName qServiceName = new QName(serviceNamespace, serviceName);
		Service service = Service.create(wsdlURL, qServiceName);
		Class<T> clazz;
		clazz = seiClass;
            
		return service.getPort(clazz);
	}
		
	
	/**
	 * Preencher com o usuario e senha do ws
	 * @param service
	 */
	public void setAuth(Object service){
		
		String user = "";
		String password = "";
		
		BindingProvider provider = (BindingProvider) service;
        provider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, user);   
        provider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, password);
		
		
	}
	
	@SuppressWarnings("unchecked")
	private String getServiceEndpointUrl() throws ConfigurationException{
		
		String endpointName = null;		
		int servicePosition = 0;
		int endpointPosition = 0;
		
		Configuration config = new XMLConfiguration("dropper-soa-client-config.xml");		
		ArrayList<String> endpointList = new ArrayList<String>(config.getList("endpoint-list.endpoint.id"));
		ArrayList<String> serviceList = new ArrayList<String>(config.getList("service-list.service.service-name"));
		
		for(String service : serviceList){
			if(service.equals(this.serviceName))
				break;
			servicePosition++;
		}
		
		endpointName = config.getString("service-list.service("+servicePosition * TOTAL_DE_TIPOS_DE_SERVICES+").endpoint-id");
		
		for(String endpoint : endpointList){
			if(endpoint.equals(endpointName))				
				break;			
			endpointPosition++;
		}
        
		return config.getString("endpoint-list.endpoint("+endpointPosition+").url");
		
		
	}
	
	
}
