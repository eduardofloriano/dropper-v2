package br.com.dropper.soa.framework.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public interface DbUtils {
	public void closeQuietly(Connection c);
	public void closeQuietly(Connection c, Statement stmt);
	public void closeQuietly(Connection c, Statement stmt, ResultSet rs);
	public void closeQuietly(Statement stmt, ResultSet rs);
   public void closeQuietly(Statement stmt);
	public void closeQuietly(ResultSet rs);
	
	public void rollbackQuietly(Connection c);

	@Deprecated
	public void closeQuietly(Connection c, ResultSet rs, Statement stmt);
}
