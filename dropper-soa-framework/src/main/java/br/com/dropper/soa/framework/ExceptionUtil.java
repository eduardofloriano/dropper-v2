package br.com.dropper.soa.framework;

import br.com.dropper.soa.modelocanonico.ErroSoaEnum;
import br.com.dropper.soa.modelocanonico.exception.AbstractSoaException;
import br.com.dropper.soa.modelocanonico.exception.EntidadeNaoLocalizadaException;
import br.com.dropper.soa.modelocanonico.exception.ParametroObrigatorioNaoInformadoException;
import br.com.dropper.soa.modelocanonico.exception.ProblemaInesperadoException;

public interface ExceptionUtil {
	public ProblemaInesperadoException converter(Exception e);
	public ProblemaInesperadoException converter(Exception e, String mensagem);	
	public AbstractSoaException converterApenasNaoSoa(Exception e);


	public <T extends EntidadeNaoLocalizadaException> T buildEntidadeNaoLocalizadaException(Class<T> exceptionClass, ErroSoaEnum erro, String entidade, String codigo);
	
	public ParametroObrigatorioNaoInformadoException buildTodosParametrosObrigatoriosException(String... parametrosObrigatorios);
	public ParametroObrigatorioNaoInformadoException buildAoMenosUmParametroObrigatorioException(String... parametrosObrigatorios);
	
	
}
