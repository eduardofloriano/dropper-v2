package br.com.dropper.soa.framework.mappers;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.dropper.soa.framework.IResponseHandler;
import br.com.dropper.soa.framework.log.ILogStrategy;
import br.com.dropper.soa.framework.log.Log4J;
import br.com.dropper.soa.modelocanonico.ErroSoaEnum;
import br.com.dropper.soa.modelocanonico.exception.ParametroObrigatorioNaoInformadoException;

@Provider
public class ParametroObrigatorioNaoInformadoFaultMapper implements ExceptionMapper<ParametroObrigatorioNaoInformadoException> {

	@Inject
	private IResponseHandler responseHandler;

	@Inject
	@Log4J
	ILogStrategy log;

	@Override
	public Response toResponse(ParametroObrigatorioNaoInformadoException e) {
		log.info(e.getMessage(), e);
		return responseHandler.buildFault("ParametroObrigatorioNaoInformadoFault", 
				"BE-"+ ErroSoaEnum.ParametroObrigatorioNaoInformado.getCodigo(), e.getFaultInfo().getMensagem(),
				e.getFaultInfo().getInstrucoes(), e);
	}
	
}
