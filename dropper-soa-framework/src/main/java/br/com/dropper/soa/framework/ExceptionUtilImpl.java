package br.com.dropper.soa.framework;

import java.lang.reflect.Constructor;
import java.util.Arrays;

import br.com.dropper.soa.modelocanonico.ErroSoaEnum;
import br.com.dropper.soa.modelocanonico.exception.AbstractSoaException;
import br.com.dropper.soa.modelocanonico.exception.EntidadeNaoLocalizadaException;
import br.com.dropper.soa.modelocanonico.exception.ParametroObrigatorioNaoInformadoException;
import br.com.dropper.soa.modelocanonico.exception.ProblemaInesperadoException;
import br.com.dropper.soa.modelocanonico.fault.DropperSoaFault;

public class ExceptionUtilImpl extends Object implements ExceptionUtil {

	@Override
	public ProblemaInesperadoException converter(Exception e) {
		String msg = "Ocorreu um erro inesperado: " + e.getMessage();
		ProblemaInesperadoException e1 = new ProblemaInesperadoException(msg, new DropperSoaFault(ErroSoaEnum.ProblemaInesperado, msg, "Entrar em contato com suporte técnico fornecendo detalhes."), e);
		return e1;
	}
	
	@Override
	public ProblemaInesperadoException converter(Exception e, String mensagem) {
		String msg = "Ocorreu um erro inesperado: " + mensagem + " -  Exception: " + e.getMessage();
		ProblemaInesperadoException e1 = new ProblemaInesperadoException(msg, new DropperSoaFault(ErroSoaEnum.ProblemaInesperado, msg, "Entrar em contato com suporte técnico fornecendo detalhes."), e);
		return e1;
	}
	

	  @Override
	   public AbstractSoaException converterApenasNaoSoa(Exception e) {
	     if(e instanceof AbstractSoaException) {
	        return (AbstractSoaException) e;
	     }
	      String msg = "Ocorreu um erro inesperado: " + e.getMessage();
	      ProblemaInesperadoException e1 = new ProblemaInesperadoException(msg, new DropperSoaFault(ErroSoaEnum.ProblemaInesperado, msg, "Entrar em contato com suporte técnico fornecendo detalhes."), e);
	      return e1;
	   }
	
	@Override
	public ParametroObrigatorioNaoInformadoException buildTodosParametrosObrigatoriosException(
			String... parametrosObrigatorios) {
		final String [] modelosMensagem = {"O parametro [%s] e obrigatorio", "Os parametros %s sao obrigatorios."};
		return buildParametroInvalidoFaultException(modelosMensagem,  parametrosObrigatorios);
	}

	@Override
	public ParametroObrigatorioNaoInformadoException buildAoMenosUmParametroObrigatorioException(
			String... parametrosObrigatorios) {
		final String [] modelosMensagem = {"O parametro [%s] e obrigatorio", "Ao menos um dos parametros %s deve ser fornecido."};
		return buildParametroInvalidoFaultException(modelosMensagem,  parametrosObrigatorios);
	}

	private ParametroObrigatorioNaoInformadoException buildParametroInvalidoFaultException(String modelosMsg[],  String... parametrosObrigatorios) {
		if(parametrosObrigatorios.length == 0) {
			throw new IllegalArgumentException("Argumentos inválidos para a construção de ParametroInvalidoFault, ao menos um parâmetro obrigatório deve ser preenchido.");
		}
		String modeloMsg;
		String parametrosStr;
		if(parametrosObrigatorios.length == 1) {
			modeloMsg = modelosMsg[0];
			parametrosStr = parametrosObrigatorios[0];
		} else {
			modeloMsg = modelosMsg[1];
			parametrosStr = Arrays.toString(parametrosObrigatorios);
		}
		String parametrosMsg = String.format(modeloMsg, parametrosStr);
		return new ParametroObrigatorioNaoInformadoException(
				parametrosMsg,
				new DropperSoaFault(ErroSoaEnum.ParametroObrigatorioNaoInformado, parametrosMsg, 
				"Verifique se os parametros entrados estao corretos"));
	}

   @Override
   public <T extends EntidadeNaoLocalizadaException> T buildEntidadeNaoLocalizadaException(Class<T> exceptionClass, ErroSoaEnum erro, String entidade, String codigo) {
      try {
    	  DropperSoaFault fault = new DropperSoaFault(erro, entidade + " com identificador: '" + codigo + "' não localizado(a)", "Verifique os parâmetros de entrada antes de tentar novamente.");
         Constructor<T> construtor = exceptionClass.getConstructor(new Class<?>[]{String.class, DropperSoaFault.class});
         T e = construtor.newInstance(fault.getMensagem(), fault);
         e.setEntidade(entidade);
         e.setCodigo(codigo);
         
         return e;
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
   }



}
