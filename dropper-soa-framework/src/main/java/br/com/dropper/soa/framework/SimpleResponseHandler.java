package br.com.dropper.soa.framework;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;

public class SimpleResponseHandler implements IResponseHandler {

	
	@Override
	public Response buildFault(String tipo, String codigo, String mensagem, String instrucao, Throwable e) {
		String stackStraceStr = formatStackTrace(e);
		return buildFault(tipo, codigo, mensagem, instrucao, "StackTrace:\n" + stackStraceStr);
	}

	@Override
	public Response buildFault(String tipo, String codigo, String mensagem, String instrucao, String detalhe) {
		
		return Response.ok().variants(Variant.encodings("windows-1252").build())
				.entity(buildFaultStr(tipo, codigo, mensagem, instrucao, detalhe)).build();
	}
	
	@Override
	public Response buildTechnicalFault(String codigo, Throwable e) {
		return buildFault("TechnicalFault", codigo, "Ocorreu uma falha inesperada", "Favor contatar o suporte tecnico", formatStackTrace(e));
	}

	@Override
	public Response buildTodosParametrosObrigatoriosResponse(String codigo, String... parametrosObrigatorios) {
		final String [] modelosMensagem = {"O parametro [%s] e obrigatorio", "Os parametros %s sao obrigatorios."};
		return buildParametroInvalidoFaultResponse(modelosMensagem, codigo, parametrosObrigatorios);
	}
	
	@Override
	public Response buildAoMenosUmParametroObrigatorioResponse(String codigo, String... parametrosObrigatorios) {
		final String [] modelosMensagem = {"O parametro [%s] e obrigatorio", "Ao menos um dos parametros %s deve ser fornecido."};
		return buildParametroInvalidoFaultResponse(modelosMensagem, codigo, parametrosObrigatorios);
	}		
	
	private Response buildParametroInvalidoFaultResponse(String modelosMsg[], String codigo, String... parametrosObrigatorios) {
		if(parametrosObrigatorios.length == 0) {
			throw new IllegalArgumentException("Argumentos inválidos para a construção de ParametroInvalidoFault, ao menos um parâmetro obrigatório deve ser preenchido.");
		}
		String modeloMsg;
		String parametrosStr;
		if(parametrosObrigatorios.length == 1) {
			modeloMsg = modelosMsg[0];
			parametrosStr = parametrosObrigatorios[0];
		} else {
			modeloMsg = modelosMsg[1];
			parametrosStr = Arrays.toString(parametrosObrigatorios);
		}
		String parametrosMsg = String.format(modeloMsg, parametrosStr);
		return buildFault("ParametroInvalidoFault", codigo, parametrosMsg,
				"Verifique se os parametros entrados estao corretos", "StackTrace:");
		
	}


	private String formatStackTrace(Throwable e) {
		StringWriter stringWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(stringWriter));
		String stackTraceFormatted; 
		stackTraceFormatted = stringWriter.toString().replace("\"", "");
		return stackTraceFormatted;
	}

	private String buildFaultStr(String tipo, String codigo, String mensagem, String instrucao, String detalhe) {
		String faultStr = String.format(
				"{\"%s\":{\"codigo\":\"%s\",\"mensagem\":\"%s\",\"instrucao\":\"%s\",\"detalhe\":\"%s\"}}", tipo,
				codigo, mensagem, instrucao, detalhe);
		return faultStr;
	}


}
