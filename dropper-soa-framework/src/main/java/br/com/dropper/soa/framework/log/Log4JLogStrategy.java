package br.com.dropper.soa.framework.log;

import javax.inject.Inject;

import org.apache.log4j.Logger;

@Log4J
public class Log4JLogStrategy implements ILogStrategy {

	@Inject
	private transient Logger _logger;

	@Override
	public void debug(String message) {
		_logger.debug(message);
	}
	
	@Override
	public void debug(String message, Throwable e) {
		_logger.debug(message, e);
	}

	@Override
	public void info(String message) {
		_logger.info(message);
	}
	
	@Override
	public void info(String message, Throwable e) {
		_logger.info(message, e);
	}

	@Override
	public void warn(String message) {
		_logger.warn(message);
	}

	@Override
	public void warn(String message, Throwable e) {
		_logger.warn(message, e);
	}	
	
	@Override
	public void error(String message) {
		_logger.error(message);
	}

	@Override
	public void error(String message, Throwable ex) {
		_logger.error(message, ex);
	}

	@Override
	public void fatal(String message) {
		_logger.fatal(message);
	}

	@Override
	public void fatal(String message, Throwable ex) {
		_logger.fatal(message, ex);
	}





}
