package br.com.dropper.soa.framework;

import java.text.SimpleDateFormat;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	private ObjectMapper objectMapper;

	public ObjectMapperContextResolver() throws Exception {
		this.objectMapper = new ObjectMapper().configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true)
				.configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false)
				.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
				//.configure(SerializationConfig.Feature.WRAP_EXCEPTIONS, false);
		this.objectMapper.setDateFormat(DATE_FORMAT);

	}
	
	@Override
	public ObjectMapper getContext(Class<?> objectType) {
		return objectMapper;
	}
}