package br.com.dropper.soa.consultarimagem.sei;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;



//@JsonRootName("ConsultarImagemRequest")
@XmlType
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class ConsultarImagemRequest {
	
	private Long codigo;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	


	
}