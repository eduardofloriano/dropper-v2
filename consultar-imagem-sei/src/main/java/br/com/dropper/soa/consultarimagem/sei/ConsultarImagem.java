package br.com.dropper.soa.consultarimagem.sei;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import br.com.dropper.soa.framework.ServiceFactory;
import br.com.dropper.soa.modelocanonico.exception.ParametroObrigatorioNaoInformadoException;
import br.com.dropper.soa.modelocanonico.exception.ProblemaInesperadoException;
import br.com.dropper.soa.modelocanonico.exception.ImagemNaoLocalizadoException;;

@WebService
public interface ConsultarImagem {

	public static final ServiceFactory<ConsultarImagem> FACTORY = new ServiceFactory<ConsultarImagem>(
			ConsultarImagem.class, "ConsultarImagem.v1", "ConsultarImagem",
			"http://v1.consultarimagem.soa.dropper.com.br/");

	@WebMethod(operationName = "ConsultarImagemRequest")
	//@ResponseWrapper(localName = "ConsultarImagemResponse")
	@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE) 
	public ConsultarImagemResponse consultar(ConsultarImagemRequest request)
			throws ParametroObrigatorioNaoInformadoException,			
			ProblemaInesperadoException,ImagemNaoLocalizadoException;
}
