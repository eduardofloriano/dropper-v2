package br.com.dropper.soa.consultarimagem.sei;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import br.com.dropper.soa.modelocanonico.Imagem;

//@JsonRootName("ConsultarImagemResponse")
@XmlType
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class ConsultarImagemResponse {
	
	private Imagem imagem;

	public Imagem getImagem() {
		return imagem;
	}

	public void setImagem(Imagem imagem) {
		this.imagem = imagem;
	}
}
