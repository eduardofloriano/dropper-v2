package br.com.dropper.soa.consultarimagem;

import java.net.MalformedURLException;

import org.apache.commons.configuration.ConfigurationException;

import br.com.dropper.soa.consultarimagem.sei.ConsultarImagem;
import br.com.dropper.soa.consultarimagem.sei.ConsultarImagemRequest;
import br.com.dropper.soa.consultarimagem.sei.ConsultarImagemResponse;
import br.com.dropper.soa.modelocanonico.exception.ImagemNaoLocalizadoException;
import br.com.dropper.soa.modelocanonico.exception.ParametroObrigatorioNaoInformadoException;
import br.com.dropper.soa.modelocanonico.exception.ProblemaInesperadoException;

public class ConsultarImagemTest {

	public static void main(String[] args) throws MalformedURLException, ProblemaInesperadoException, ConfigurationException, ParametroObrigatorioNaoInformadoException, ImagemNaoLocalizadoException
	{
	
		
		ConsultarImagem servico = ConsultarImagem.FACTORY.lookupService();
		ConsultarImagemRequest request = new ConsultarImagemRequest();
		try {
			ConsultarImagemResponse response = servico.consultar(request);
			System.out.println(response.getImagem().getNome()); 
		} catch(ParametroObrigatorioNaoInformadoException e) {
			System.out.println("Parâmetro obrigatório não informado: " + e.getMessage());
		}
	}

}
