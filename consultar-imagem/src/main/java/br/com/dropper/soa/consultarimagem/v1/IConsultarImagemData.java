package br.com.dropper.soa.consultarimagem.v1;



import br.com.dropper.soa.modelocanonico.Imagem;
import br.com.dropper.soa.modelocanonico.exception.ImagemNaoLocalizadoException;
import br.com.dropper.soa.modelocanonico.exception.ProblemaInesperadoException;


public interface IConsultarImagemData {
	public Imagem consultarImagemPorCodigo(Long codigo)
			throws ImagemNaoLocalizadoException,
			ProblemaInesperadoException;

	
}
