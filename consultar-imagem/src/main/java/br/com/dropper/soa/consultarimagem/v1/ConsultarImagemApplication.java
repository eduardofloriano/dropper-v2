package br.com.dropper.soa.consultarimagem.v1;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class ConsultarImagemApplication extends Application {

}
