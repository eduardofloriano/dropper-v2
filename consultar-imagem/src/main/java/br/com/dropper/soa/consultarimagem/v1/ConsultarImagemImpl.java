package br.com.dropper.soa.consultarimagem.v1;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.dropper.soa.consultarimagem.sei.ConsultarImagem;
import br.com.dropper.soa.consultarimagem.sei.ConsultarImagemRequest;
import br.com.dropper.soa.consultarimagem.sei.ConsultarImagemResponse;
import br.com.dropper.soa.framework.ExceptionUtil;
import br.com.dropper.soa.framework.IResponseHandler;
import br.com.dropper.soa.framework.db.NativeDB;
import br.com.dropper.soa.modelocanonico.Imagem;
import br.com.dropper.soa.modelocanonico.exception.ImagemNaoLocalizadoException;
import br.com.dropper.soa.modelocanonico.exception.ParametroObrigatorioNaoInformadoException;
import br.com.dropper.soa.modelocanonico.exception.ProblemaInesperadoException;





@WebService(endpointInterface = "br.com.dropper.soa.consultarimagem.sei.ConsultarImagem", serviceName = "ConsultarImagem")
@Path("consultarImagem")
public class ConsultarImagemImpl implements ConsultarImagem{

	
	@Inject
	@RequestScoped
	private IResponseHandler responseHandler;
	
	@Inject
	private ExceptionUtil exceptionUtil;
	
	@Inject
	@NativeDB
	@RequestScoped
	private IConsultarImagemData consultarImagemData;

	@Override
	public ConsultarImagemResponse consultar(ConsultarImagemRequest request)
			throws ParametroObrigatorioNaoInformadoException,			
			ProblemaInesperadoException,
			ImagemNaoLocalizadoException {
		return doConsultar(request);
	}
	
	

	private ConsultarImagemResponse doConsultar(ConsultarImagemRequest request) throws  ParametroObrigatorioNaoInformadoException, ProblemaInesperadoException,  ImagemNaoLocalizadoException {
		
		ParametroObrigatorioNaoInformadoException err = validaRequest(request);
		if (err != null) {
			throw err; 
		}
		
		Long codigo = request.getCodigo();
		
		Imagem imagem = consultarImagemData.consultarImagemPorCodigo(codigo);
		
		ConsultarImagemResponse response = new ConsultarImagemResponse();
		response.setImagem(imagem);

		return response;
		
	}
	
	
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{imagem}/{codigo}")
	public ConsultarImagemResponse consultar(@PathParam("codigo") Long codigo)
			throws  ParametroObrigatorioNaoInformadoException,  ProblemaInesperadoException, ImagemNaoLocalizadoException {
		
		ConsultarImagemRequest request = new ConsultarImagemRequest();
		request.setCodigo(codigo);
		return doConsultar(request);
		
	}
	
	private ParametroObrigatorioNaoInformadoException validaRequest(ConsultarImagemRequest request) throws ParametroObrigatorioNaoInformadoException {
		
		if (request.getCodigo() == null || request.getCodigo() == 0) {
			return exceptionUtil.buildTodosParametrosObrigatoriosException("codigo");
		}		
		return null;
	}	
	
}
