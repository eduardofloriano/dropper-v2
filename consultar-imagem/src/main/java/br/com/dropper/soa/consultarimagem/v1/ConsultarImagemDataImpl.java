package br.com.dropper.soa.consultarimagem.v1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;

import br.com.dropper.soa.framework.ExceptionUtil;
import br.com.dropper.soa.framework.db.DbUtilsImpl;
import br.com.dropper.soa.framework.db.NativeDB;
import br.com.dropper.soa.modelocanonico.Imagem;
import br.com.dropper.soa.modelocanonico.exception.AbstractSoaException;
import br.com.dropper.soa.modelocanonico.exception.ImagemNaoLocalizadoException;
import br.com.dropper.soa.modelocanonico.exception.ProblemaInesperadoException;

@NativeDB
public class ConsultarImagemDataImpl implements IConsultarImagemData {

	@Resource(lookup = "java:jboss/datasources/DropperDataSource")
	private DataSource dataSource;

	@Inject
	private DbUtilsImpl dbUtils;

	@Inject
	ExceptionUtil exceptionUtil;
	
	private Connection getConnection() throws SQLException,
			ProblemaInesperadoException {
		Connection c;
		try {
			c = dataSource.getConnection();
			c.setAutoCommit(false);
		} catch (Exception e) {
			if (e instanceof AbstractSoaException) {
				throw e;
			}
			throw exceptionUtil.converter(e);
		}

		return c;
	}

	

	public Imagem consultarImagemPorCodigo(Long codigo) throws ImagemNaoLocalizadoException, ProblemaInesperadoException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection c = null;
		
		
		Imagem imagem = new Imagem();
		
		try {
			c = getConnection();
			stmt = c.prepareStatement("SELECT CD_IMAGEM, NM_IMAGEM, TAM_IMAGEM, BLOB_IMAGEM FROM IMAGEM WHERE CD_IMAGEM = ? ");
			stmt.setLong(1, codigo);
			rs = stmt.executeQuery();

			
			if (rs.next()) {
				imagem.setCodigo(rs.getLong("CD_IMAGEM"));
				imagem.setNome(rs.getString("NM_IMAGEM"));
				imagem.setTamanho(rs.getLong("TAM_IMAGEM"));
				imagem.setConteudo(rs.getBytes("BLOB_IMAGEM"));
			} else {
				String mensagem = "A Imagem com codigo: " + codigo + " nao foi encontrada na base de dados";
				throw new ImagemNaoLocalizadoException(mensagem);
			}

		} catch (Exception e) {			
			throw exceptionUtil.converter(e);
		} finally {
			dbUtils.closeQuietly(c, stmt, rs);
		}

		return imagem;
	}

}
